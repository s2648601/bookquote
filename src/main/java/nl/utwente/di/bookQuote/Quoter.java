package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String isbn){
        HashMap<String, Double> map = new HashMap<>();
        map.put("1", 10.0);
        map.put("2", 45.0);
        map.put("3", 20.0);
        map.put("4", 35.0);
        map.put("5", 50.0);

        Double result = map.get(isbn);
        if (map.get(isbn) == null){
            result = 0.0;
        }
        return result;
    }
}